
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

 
 function generirajVzorec() {
    generirajPodatke(1);
    generirajPodatke(2);
    generirajPodatke(3);
}


 
 
 
function generirajPodatke(stPacienta) {
    ehrId = "";
     var mass = [
        ["40", "41", "42", "43", "44", "45", "46", "47", "48", "49"],
        ["50", "51", "52", "53", "54", "55", "56", "57", "58", "59"],
        ["60", "61", "62", "63", "64", "65", "66", "67", "68", "69"],
        ["70", "71", "72", "73", "74", "75", "76", "77", "78", "79"],
        ["80", "81", "82", "83", "84", "85", "86", "87", "88", "89"],
        ["90", "91", "92", "93", "94", "95", "96", "97", "98", "99"],
        ["100", "101", "102", "103", "104", "105", "106", "107", "108", "109"],
        ["110", "111", "112", "113", "114", "115", "116", "117", "118", "119"],
    ];

    var heights = [
        ["140", "141", "142", "143", "144", "145", "146", "147", "148", "149"],
        ["150", "151", "152", "153", "154", "155", "156", "157", "158", "159", "160"],
        ["161", "162", "163", "164", "165", "166", "167", "168", "169", "170"],
        ["171", "172", "173", "174", "175", "176", "177", "178", "179", "180"],
        ["180", "181", "182", "183", "184", "185", "186", "187", "188", "189"],
        ["190", "191", "192", "193", "194", "195", "196", "197", "198", "199"],
        
    ];
  
    var sisTlak = [
        ["40", "41", "42", "43", "44", "45", "46", "47", "48", "49"],
        ["50", "51", "52", "53", "54", "55", "56", "57", "58", "59"],
        ["60", "61", "62", "63", "64", "65", "66", "67", "68", "69"],
        ["70", "71", "72", "73", "74", "75", "76", "77", "78", "79"],
        ["80", "81", "82", "83", "84", "85", "86", "87", "88", "89"],
        ["90", "91", "92", "93", "94", "95", "96", "97", "98", "99"],
        ["100", "101", "102", "103", "104", "105", "106", "107", "108", "109"],
        ["110", "111", "112", "113", "114", "115", "116", "117", "118", "119"],
        ["120", "121", "122", "123", "124", "125", "126", "127", "128", "129"],
        ["130", "131", "132", "133", "134", "135", "136", "137", "138", "139"],
        ["140", "141", "142", "143", "144", "145", "146", "147", "148", "149"],
        ["150", "151", "152", "153", "154", "155", "156", "157", "158", "159", "160"],
        ["161", "162", "163", "164", "165", "166", "167", "168", "169", "170"],
        ["171", "172", "173", "174", "175", "176", "177", "178", "179", "180"],
        ["180", "181", "182", "183", "184", "185", "186", "187", "188", "189"],
        ["190", "191", "192", "193", "194", "195", "196", "197", "198", "199"],
        
    ];
    var diaTlak = [
        ["40", "41", "42", "43", "44", "45", "46", "47", "48", "49"],
        ["50", "51", "52", "53", "54", "55", "56", "57", "58", "59"],
        ["60", "61", "62", "63", "64", "65", "66", "67", "68", "69"],
        ["70", "71", "72", "73", "74", "75", "76", "77", "78", "79"],
        ["80", "81", "82", "83", "84", "85", "86", "87", "88", "89"],
        ["90", "91", "92", "93", "94", "95", "96", "97", "98", "99"],
        ["100", "101", "102", "103", "104", "105", "106", "107", "108", "109"],
        ["110", "111", "112", "113", "114", "115", "116", "117", "118", "119"],
        ["120", "121", "122", "123", "124", "125", "126", "127", "128", "129"],
        ["130", "131", "132", "133", "134", "135", "136", "137", "138", "139"],
        ["140", "141", "142", "143", "144", "145", "146", "147", "148", "149"],
        ["150", "151", "152", "153", "154", "155", "156", "157", "158", "159", "160"],
        ["161", "162", "163", "164", "165", "166", "167", "168", "169", "170"],
        ["171", "172", "173", "174", "175", "176", "177", "178", "179", "180"],
        ["180", "181", "182", "183", "184", "185", "186", "187", "188", "189"],
        ["190", "191", "192", "193", "194", "195", "196", "197", "198", "199"],
    ];

    switch (stPacienta) {
        case 1:
            ehrId = ustvariEHRvzorec('Gargamel', 'Smrkec');
            for (var i = 0; i < 10; i++) {
                var random = Math.round((Math.random() * 115) + 1);
                var dateTime =new Date(1997 + i, (2 + random) % 3 + 3, 15, (25 + random * random) % 24, (39 + random) % 24);
                dodajPodatkeVzorca(ehrId, {
                    visina: heights[1][i],
                    teza: mass[1][i],
                    diastolicni: diaTlak[1][i],
                    sistolicni: sisTlak[1][i],
                    merilec: 'Papa Strumf'
                }); }
            break;
        case 2:
            ehrId = ustvariEHRvzorec('Wade', 'Wilson');
             for (var i = 0; i < 10; i++) {
                var random = Math.round((Math.random() * 115) + 1);
                var dateTime = new Date(1997 + i, (2 + random) % 3 + 3, 15, (18 + random * random) % 24, (39 + random) % 24);
                dodajPodatkeVzorca(ehrId, {
                    visina: heights[1][i],
                    teza: mass[1][i],
                    diastolicni: diaTlak[1][i],
                    sistolicni: sisTlak[1][i],
                    merilec: 'Deadpool'
                }); }
            break;
        case 3:
            ehrId = ustvariEHRvzorec('Kenny', 'Mccormic')
             for (var i = 0; i < 10; i++) {
                var random =Math.round((Math.random() * 115) + 1);
                var dateTime = new Date(1997 + i, (2 + random) % 3 + 3, 15, (18 + random * random) % 24, (39 + random) % 24);
                dodajPodatkeVzorca(ehrId, {
                    visina: heights[1][i],
                    teza: mass[1][i],
                    diastolicni: diaTlak[1][i],
                    sistolicni: sisTlak[1][i],
                    merilec: 'Dr. Alphonse Mephesto'
                }); }
            break; }
            return ehrId;
}

function ustvariEHRvzorec(ime, priimek) {
    sessionId = getSessionId();
    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    })
    var odgovor = $.ajax({
        url: baseUrl + '/ehr',
        async: false,
        type: 'POST',
        success: function(data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                partyAdditionalInfo: [{
                    key: "ehrId",
                    value: ehrId
                }] };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function(party) {
                    $('#kreirajOpozorilo').text('Uspešno kreiran EHR ID za: ' + ime + ' ' + priimek + ' (' + ehrId + ')');
                    $('#dodajEHR').val(ehrId);
                    $('#preberiEHR').val(ehrId);
                    console.log("dvkddkf");
                },
                error: function(err) {
                    $('#kreirajOpozorilo').text('Napaka!');
                }
            }); }  });
    console.log(odgovor);
    return odgovor.responseJSON.ehrId;
}




function zgodovinaIzpis() {
    sessionId = getSessionId();

    var ehrId = $("#preberiEHR").val();
    $('#podatkiPacienta tbody').empty();

    if (!ehrId || ehrId.trim().length == 0) {
        $("#opozoriloBranja").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prosim vnesite EHR ID!");
    } else {
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure?" + $.param({
                limit: 25 }),
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId},
            success: function(tlaki) {
                $.ajax({
                    url: baseUrl + "/view/" + ehrId + "/" + "weight",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },
                    success: function(res) {
                        if (res.length > 0) {
                            var zaGraf = [];
                            var results = "<table class='table table-striped table-hover>";
                            for (var i = 0; i < res.length; i++) {
                                results =
                                    ' \
                                    <tr> \
                                    <th scope="row">' + (i + 1) + '</td> \
                                    <td>' + tlaki[i].time.substring(0, 16) + '</td> \
                                    <td>' + tlaki[i].systolic + '/' + tlaki[i].diastolic + '<span style="margin-left: 10px; color:#1010"></span></td> \
                                    <td>' + res[i].weight + ' ' + res[i].unit + '</td> \
                                    </tr> '
                                results += "</table>";
                                $("#podatkiPacienta tbody").append(results);
                                zaGraf.push(tlaki[i].systolic);
                            }
                            Array.prototype.max = function() {
                                return Math.max.apply(null, this);
                                
                                if (tlaki[i].systolic > 139 || tlaki[i].systolic==0) {
                                $("#opozoriloBranja").html(
                                "<span class='obvestilo label label-warning fade-in'>" +
                                "Find nearest hospital!</span>");
                            } 
                            };izrisiGraf(zaGraf);
                           // console.log(tlaki[i].systolic);
                        }else {
                            $("#opozoriloBranja").html(
                                "<span class='obvestilo label label-warning fade-in'>" +
                                "Input valid EHR ID!</span>");
                        } },
                });
            },
        });
    }
}

function ustvariEHRpacienta() {
    $('#addEhrId').val(ustvariEHRvzorec($('#ustvariIme').val(), $('#ustvariPriimek').val()));
    $('#ustvariIme').val('');
    $('#ustvariPriimek').val('');

}

function izrisiGraf(data) {
    $(function() {
        $('#grafProstor').highcharts({
            title: {
                text: 'Blood preasure graph',
                x: -20 },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun','Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
            }, yAxis: {
                title: {
                    text: 'Tlak (mmHg)' },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'}]},
            tooltip: {
                valueSuffix: 'mmHg'},
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0},
            series: [{
                name: 'Krvni tlak',
                data: data}]
        });});}
function dodajPodatkeVzorca(ehrId, data) {
    var sessionId = getSessionId();
    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });
    var data = {
        "ctx/language": "en",
        "ctx/territory": "SI",
        "vital_signs/height_length/any_event/body_height_length": data.visina,
        "vital_signs/body_weight/any_event/body_weight": data.teza,
        "vital_signs/blood_pressure/any_event/systolic": data.sistolicni,
        "vital_signs/blood_pressure/any_event/diastolic": data.diastolicni,};
    var parametriZahteve = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: data.merilec
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function(odgovor) {
            $('#dodajOpozorilo').text('Podatki so bili dodani');
        },
        error: function(err) {
            $('#dodajOpozorilo').text('Napaka! Popravite podatke');
        }
    });
}

function dodajPodatkeGumb() {
    var date = new Date();
    dodajPodatkeVzorca($('#dodajEHR').val(), {
        visina: $('#dodajVisina').val(),
        teza: $('#dodajTeza').val(),
        sistolicni: $('#dodajSistolicni').val(),
        diastolicni: $('#dodajDiastolicni').val(),
        merilec: 'Trenutni uporabnik'
    });
    $('#dodajVisina').val('');
    $('#dodajTeza').val('');
    $('#dodajSistolicni').val('');
    $('#dodajDiastolicni').val('');
}

$(window).load(function() {
    $("#addIzbiroEHR").change(function() {
        $('#dodajEHR').val($('#addIzbiroEHR').val());
    });
    $("#dodajIzbraniEHR").change(function() {
        // console.log($('#branje_selectEhrId').val());
        $('#preberiEHR').val($('#dodajIzbraniEHR').val());
    });

});

